# BioMedIt Infrastructure

Live version: https://biomedit.gitlab.io/presentations/biomedit

## Use locally

One time dev setup:

```bash
npm install
ln -s node_modules/reveal.js/{plugin,dist} .
```

Then open [index.html](index.html) in your browser.
